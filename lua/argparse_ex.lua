-- https://argparse.readthedocs.io/en/stable/index.html
-- Require "argparse": luarocks install argparse

local argparse = require "argparse"

local parser = argparse() {
   name = "script",
   description = "A testing script.",
   epilog = "For more info, see http://example.com."
}
--[[
local parser = argparse("script",
			"A testing script.",
			"For more info, see http://example.com.")
]]--

parser:option("-i --input", "Input file.", "a.in")
parser:option "-o" "--output"
   :description "Output file."
   :default "a.out"
parser:option "-p"
   :default "password"
   :defmode "arg"

local args = parser:parse()

print("Input:    " .. args.input)
print("Output:   " .. args.output)
if args.p then
   print("Password: " .. args.p)
end

