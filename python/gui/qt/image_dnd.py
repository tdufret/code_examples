import sys
import os
from PySide2 import QtGui, QtCore
from PySide2 import QtWidgets


class Example(QtWidgets.QWidget):

    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        current_folder_path = os.path.dirname(os.path.abspath(__file__))
        self.img_fold = os.path.join(current_folder_path,
                                     r'../../../dataset/bib')
        self.thumb_fold = os.path.join(current_folder_path,
                                     r'../../../dataset/bib/thumbs')

        # Create a parent widget for the dnd source grid
        # It will be used to map a scroll bar
        self.dnd_src_grid_parent_wgt = QtWidgets.QGroupBox('Drag n drop source')
        # Create a grid layout for drag n drop source
        self.dnd_src_grid = QtWidgets.QGridLayout(self.dnd_src_grid_parent_wgt)
        self.dnd_src_grid.setAlignment(QtCore.Qt.AlignHCenter)
        # Create a scroll area for dnd source grid
        self.dnd_src_grid_scrollarea = QtWidgets.QScrollArea()
        self.dnd_src_grid_scrollarea.setWidgetResizable(True)
        # Map dnd_src_grid_parent_wgt into the scroll area
        self.dnd_src_grid_scrollarea.setWidget(self.dnd_src_grid_parent_wgt)

        if True:
            self.dnd_dest_grid = dropGrid('Drag n drop destination', parent=None)
        else:
            # Create a parent widget for the dnd dest grid
            # It will be used to map a scroll bar
            self.dnd_dest_grid_parent_wgt = QtWidgets.QGroupBox('Drag n drop destination')
            # Create a grid layout for drag n drop dest
            self.dnd_dest_grid = QtWidgets.QGridLayout(
                self.dnd_dest_grid_parent_wgt)
            self.dnd_dest_grid.setAlignment(QtCore.Qt.AlignHCenter)
            # Create a scroll area for dnd dest grid
            self.dnd_dest_grid_scrollarea = QtWidgets.QScrollArea()
            self.dnd_dest_grid_scrollarea.setWidgetResizable(True)
            # Map dnd_dest_grid_parent_wgt into the scroll area
            self.dnd_dest_grid_scrollarea.setWidget(self.dnd_dest_grid_parent_wgt)

            self.dnd_dest_grid_parent_wgt.setAcceptDrops(True)

        self.dnd_hbox_wgt = QtWidgets.QWidget()
        self.dnd_hbox = QtWidgets.QHBoxLayout(self.dnd_hbox_wgt)
        self.dnd_hbox.addWidget(self.dnd_src_grid_scrollarea)
        self.dnd_hbox.addWidget(self.dnd_dest_grid)

        # Create a button to load images
        self.load_button = QtWidgets.QPushButton('&Load', self)
        self.load_button.clicked.connect(self.fill_grid)

        # create a main layout
        self.main_widget_layout = QtWidgets.QVBoxLayout(self)
        self.main_widget_layout.addWidget(self.load_button)
        self.main_widget_layout.addWidget(self.dnd_hbox_wgt)

        self.setGeometry(50, 50, 800, 800)
        self.setWindowTitle('Image drag n drop test')
        self.show()

    def fill_grid(self):
        img_index = 0
        nb_cols = 3
        for img in os.listdir(self.img_fold):
            img_path = os.path.join(self.img_fold, img)
            thumb_path = os.path.join(self.thumb_fold, img)
            if not os.path.exists(thumb_path):
                if not os.path.exists(self.thumb_fold):
                    os.makedirs(self.thumb_fold)
                pixmap = QtGui.QPixmap(img_path)
                pixmap = pixmap.scaledToWidth(100)
                pixmap.save(thumb_path)
            pixmap = QtGui.QPixmap(thumb_path)
            #lbl = QtWidgets.QLabel(self)
            lbl = dragLabel(self)
            lbl.setPixmap(pixmap)
            lbl.setObjectName(thumb_path)
            #lbl.setDragEnabled(True)

            row = img_index // nb_cols
            col = img_index % nb_cols
            self.dnd_src_grid.addWidget(lbl, row, col)
            img_index += 1


class dropGrid(QtWidgets.QGroupBox):

    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.setAcceptDrops(True)
        self.img_index = 0
        self.nb_cols = 3
        
        self.grid = QtWidgets.QGridLayout()
        # Create a scroll area for the grid
        #self.grid_scrollarea = QtWidgets.QScrollArea()
        #self.grid_scrollarea.setWidgetResizable(True)
        # Map self into the scroll area
        #self.grid_scrollarea.setWidget(self)
        self.setLayout(self.grid)

    def dragEnterEvent(self, e):
        #if e.mimeData().hasFormat('text/plain'):
        #    e.accept()
        #else:
        #    e.ignore()
        print('e.mimeData(): {}'.format(e.mimeData()))
        e.accept()
        #{pass

    def dropEvent(self, e):
        if e.mimeData().hasText:
            droped_widget_name = e.mimeData().text()
            print('droped_widget_name: {}'.format(droped_widget_name))
        else:
            print('droped object has no text...')
            return

        # Move widget from source grid to dest grid
        # i.e. update source and dest grid image list and refresh ?
        thumb_path = droped_widget_name
        if not os.path.exists(thumb_path):
            print('Error, file {} does not exists...'.format(thumb_path))
        pixmap = QtGui.QPixmap(thumb_path)
        lbl = dragLabel(self)
        lbl.setPixmap(pixmap)
        lbl.setObjectName(thumb_path)

        self.addWidget(lbl)

    def addWidget(self, widget):
        row = self.img_index // self.nb_cols
        col = self.img_index % self.nb_cols
        self.grid.addWidget(widget, row, col)
        self.img_index += 1


class dragLabel(QtWidgets.QLabel):

    def __init__(self, parent):
        super().__init__(parent)

    def mouseMoveEvent(self, e):
        if e.buttons() != QtCore.Qt.RightButton:
            return

        mimeData = QtCore.QMimeData()
        mimeData.setText(self.objectName())
        drag = QtGui.QDrag(self)
        drag.setMimeData(mimeData)
        drag.setHotSpot(e.pos() - self.rect().topLeft())

        dropAction = drag.start(QtCore.Qt.MoveAction)

    def mousePressEvent(self, e):
        #QtGui.QPushButton.mousePressEvent(self, e)
        if e.button() == QtCore.Qt.LeftButton:
            print('press')


def main():
    app = QtWidgets.QApplication(sys.argv)
    ex = Example()
    #ex.fill_grid()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
