import sys
import os
from PySide2 import QtGui, QtCore
from PySide2 import QtWidgets

class Example(QtWidgets.QWidget):

    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        current_folder_path = os.path.dirname(os.path.abspath(__file__))
        self.img_fold = os.path.join(current_folder_path, r'../../../dataset/bib')

        self.widget_layout = QtWidgets.QVBoxLayout(self)
        self.scrollarea = QtWidgets.QScrollArea()
        self.scrollarea.setWidgetResizable(True)
        self.widget_layout.addWidget(self.scrollarea)
        self.widget = QtWidgets.QWidget()
        #self.layout = QtWidgets.QVBoxLayout(self.widget)
        self.layout = QtWidgets.QGridLayout(self.widget)
        self.scrollarea.setWidget(self.widget)

        self.layout.setAlignment(QtCore.Qt.AlignHCenter)

        self.setGeometry(300, 300, 280, 170)
        self.setWindowTitle('Image viewer')
        self.show()


    def fill_grid(self):
        img_index = 0
        nb_cols = 3
        for img in os.listdir(self.img_fold):
            img_path = os.path.join(self.img_fold, img)
            pixmap = QtGui.QPixmap(img_path)
            pixmap = pixmap.scaledToWidth(100)
            lbl = QtWidgets.QLabel(self)
            lbl.setPixmap(pixmap)

            row = img_index // nb_cols
            col = img_index % nb_cols
            self.layout.addWidget(lbl, row, col)
            img_index += 1

def main():

    app = QtWidgets.QApplication(sys.argv)
    ex = Example()
    ex.fill_grid()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()