import paramiko

host = '192.168.0.5'
user = 'toto'
password = 'pouet'

# configure a ssh client
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy)
# Open a remote shell
client.connect(host, username=user, password=password)

# Excecute a shell command on remote host
stdin, stdout, stderr = client.exec_command('ls -l')
for line in stdout:
    print('... ' + line.strip('\n'))

# Excecute a shell command on remote host
stdin, stdout, stderr = client.exec_command('lscpu')
for line in stdout:
    print('... ' + line.strip('\n'))

# Close the remote shell
client.close()
